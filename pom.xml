<?xml version="1.0" encoding="UTF-8"?>
<!--
 SPDX-FileCopyrightText: 2011-2012 Bull S.A.S., 2021 OW2

 SPDX-License-Identifier: Apache-2.0
-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.ow2</groupId>
    <artifactId>ow2</artifactId>
    <version>2.0.1-SNAPSHOT</version>
    <packaging>pom</packaging>

    <name>OW2</name>
    <description>
        OW2 is an open source community committed to making available to everyone
        the best and most reliable open source software, including generic enterprise applications
        and cloud computing technologies. The mission of OW2 is to
        i) develop open source code for middleware, generic enterprise applications and cloud computing and
        ii) to foster a vibrant community and business ecosystem.
    </description>

    <!-- License of this POM -->
    <licenses>
        <license>
            <name>The Apache Software License, Version 2.0</name>
            <url>https://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <!-- Consortium description -->
    <organization>
        <name>OW2</name>
        <url>https://www.ow2.org</url>
    </organization>

    <!-- Project repository on GitLab -->
    <url>https://gitlab.ow2.org/ow2-lifecycle/ow2-parent-pom/</url>

    <!-- Who worked on this project? -->
    <developers>
        <developer>
            <id>sauthieg</id>
            <name>Guillaume Sauthier</name>
            <email>guillaume.sauthier@ow2.org</email>
        </developer>
        <developer>
            <id>amottier</id>
            <name>Antoine Mottier</name>
            <email>antoine.mottier@ow2.org</email>
        </developer>
    </developers>

    <properties>
        <!-- Avoid the annoying warning -->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

        <!-- Use properties so they could be overridden if needed -->
        <ow2DistMgmtSnapshotsUrl>https://repository.ow2.org/nexus/content/repositories/snapshots
        </ow2DistMgmtSnapshotsUrl>
        <ow2DistMgmtReleasesUrl>https://repository.ow2.org/nexus/service/local/staging/deploy/maven2
        </ow2DistMgmtReleasesUrl>

        <!-- Properties needed in the usage of Maven release plugin with configuration packaged in org.ow2:maven-source-assemblies dependency -->
        <!-- id of the assembly configuration as defined in `source-release.xml` packaged in maven-source-assemblies -->
        <ow2SourceAssemblyDescriptorRef>source-release</ow2SourceAssemblyDescriptorRef>
        <!-- Version of OW2 Maven artifact org.ow2:maven-source-assemblies that store Maven assembly configuration -->
        <maven-source-assemblies.version>2.0.0</maven-source-assemblies.version>

        <!-- Name of OW2 profile enabled by Maven release plugin when performing a release -->
        <ow2ReleaseProfiles>ow2-release</ow2ReleaseProfiles>

        <!-- Used plugins default versions (make them overridable) -->
        <maven-assembly-plugin.version>3.3.0</maven-assembly-plugin.version>
        <maven-gpg-plugin.version>3.0.1</maven-gpg-plugin.version>
        <maven-javadoc-plugin.version>3.3.2</maven-javadoc-plugin.version>
        <maven-source-plugin.version>3.2.1</maven-source-plugin.version>
        <maven-release-plugin.version>2.5.3</maven-release-plugin.version>
        <maven-enforcer-plugin.version>3.0.0</maven-enforcer-plugin.version>
        <maven-clean-plugin.version>3.1.0</maven-clean-plugin.version>
        <maven-resources-plugin.version>3.2.0</maven-resources-plugin.version>
        <maven-compiler-plugin.version>3.8.1</maven-compiler-plugin.version>
        <maven-surefire-plugin.version>2.22.3</maven-surefire-plugin.version>
        <maven-jar-plugin.version>3.2.2</maven-jar-plugin.version>
        <maven-install-plugin.version>2.5.2</maven-install-plugin.version>
        <maven-dependency-plugin.version>3.2.0</maven-dependency-plugin.version>
        <versions-maven-plugin.version>2.9.0</versions-maven-plugin.version>
        <maven-deploy-plugin.version>2.8.2</maven-deploy-plugin.version>
        <maven-site-plugin.version>3.11.0</maven-site-plugin.version>

        <!-- Freeze required Maven version to be able to do reproducible builds -->
        <maven.version>3.8.3</maven.version>
    </properties>

    <!-- Source Code Management -->
    <scm>
        <connection>scm:git:git@gitlab.ow2.org:ow2-lifecycle/ow2-parent-pom.git</connection>
        <developerConnection>scm:git:git@gitlab.ow2.org:ow2-lifecycle/ow2-parent-pom.git</developerConnection>
        <url>https://gitlab.ow2.org/ow2-lifecycle/ow2-parent-pom/</url>
        <tag>HEAD</tag>
    </scm>

    <!-- Distribution -->
    <distributionManagement>

        <!-- Site omitted - each project must provide their own -->

        <!-- Release Repository (with staging):
             Refer to this server in your settings.xml using the 'ow2.release' ID -->
        <repository>
            <id>ow2.release</id>
            <name>OW2 Maven Releases Repository</name>
            <url>${ow2DistMgmtReleasesUrl}</url>
        </repository>

        <!-- Snapshots Repository:
             Refer to this server in your settings.xml using the 'ow2.snapshot' ID -->
        <snapshotRepository>
            <id>ow2.snapshot</id>
            <name>OW2 Maven Snapshots Repository</name>
            <url>${ow2DistMgmtSnapshotsUrl}</url>
        </snapshotRepository>
    </distributionManagement>

    <!-- ============================================================================================
       - TODO Remove the snapshots repositories from the super pom
       - Explanation:
       -  http://www.sonatype.com/people/2010/03/why-external-repos-are-being-phased-out-of-central/
       ============================================================================================ -->

    <!-- Plugin Repositories -->
    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      - Used to search plugins, plugins dependencies and build extensions
      - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
    <pluginRepositories>
        <!-- Only list the snapshot repository here since the
             main repository is synchronized to central -->
        <pluginRepository>
            <id>ow2-plugin-snapshot</id>
            <name>OW2 Snapshot Plugin Repository</name>
            <url>https://repository.ow2.org/nexus/content/repositories/snapshots</url>
            <releases>
                <enabled>false</enabled>
            </releases>
        </pluginRepository>
    </pluginRepositories>

    <!-- Repositories -->
    <repositories>

        <!-- Only list the snapshot repository here since the
             main repository is synchronized to central -->
        <repository>
            <id>ow2-snapshot</id>
            <name>OW2 Snapshot Repository</name>
            <url>https://repository.ow2.org/nexus/content/repositories/snapshots</url>
            <releases>
                <enabled>false</enabled>
            </releases>
        </repository>
    </repositories>

    <build>
        <plugins>

            <!-- Enforce Maven version usage: excludes some that are known with defects -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <executions>
                    <execution>
                        <id>enforce-maven</id>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <failFast>true</failFast>
                    <rules>
                        <requireMavenVersion>
                            <!-- Using a range version with a unique version number to avoid default behaviour (i.e.: >=) -->
                            <version>[${maven.version}]</version>
                            <message>Force Maven version to aim for reproducible builds.</message>
                        </requireMavenVersion>
                    </rules>
                </configuration>
            </plugin>
            <!-- Display if some plugins updates are available -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>versions-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>validate</phase>
                        <goals>
                            <goal>display-property-updates</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <!-- Configure Git tag format to use only project version number when releasing using Maven release plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <configuration>
                    <!-- Avoid inclusion of project name in tag name -->
                    <tagNameFormat>@{project.version}</tagNameFormat>
                </configuration>
            </plugin>

        </plugins>

        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-enforcer-plugin</artifactId>
                    <version>${maven-enforcer-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-clean-plugin</artifactId>
                    <version>${maven-clean-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>${maven-resources-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>${maven-compiler-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>${maven-surefire-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-jar-plugin</artifactId>
                    <version>${maven-jar-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-install-plugin</artifactId>
                    <version>${maven-install-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-dependency-plugin</artifactId>
                    <version>${maven-dependency-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-source-plugin</artifactId>
                    <version>${maven-source-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-javadoc-plugin</artifactId>
                    <version>${maven-javadoc-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-gpg-plugin</artifactId>
                    <version>${maven-gpg-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-assembly-plugin</artifactId>
                    <version>${maven-assembly-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-deploy-plugin</artifactId>
                    <version>${maven-deploy-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-site-plugin</artifactId>
                    <version>${maven-site-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>versions-maven-plugin</artifactId>
                    <version>${versions-maven-plugin.version}</version>
                    <executions>
                        <execution>
                            <phase>validate</phase>
                            <goals>
                                <goal>display-property-updates</goal>
                                <goal>display-plugin-updates</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <!-- Default configuration of the maven-release-plugin:
                     * 'release' profile not used
                     * use 'ow2-release' profile
                   -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-release-plugin</artifactId>
                    <version>${maven-release-plugin.version}</version>
                    <configuration>
                        <mavenExecutorId>forked-path</mavenExecutorId>

                        <!-- Enable OW2 specific release profile -->
                        <releaseProfiles>${ow2ReleaseProfiles}</releaseProfiles>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <profiles>
        <profile>
            <id>ow2-release</id>
            <build>
                <plugins>
                    <!-- - - - - - - - - - - - - - - - - - - - - -->
                    <!--  Attach the sources to the project      -->
                    <!-- - - - - - - - - - - - - - - - - - - - - -->
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-source-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>attach-sources</id>
                                <goals>
                                    <goal>jar-no-fork</goal>
                                </goals>
                            </execution>
                        </executions>

                    </plugin>

                    <!-- - - - - - - - - - - - - - - - - - - - - -->
                    <!--  Attach the javadoc to the project      -->
                    <!-- - - - - - - - - - - - - - - - - - - - - -->
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-javadoc-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>attach-javadocs</id>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>

                    <!-- - - - - - - - - - - - - - - - - - - - - -->
                    <!--  Sign all the artifacts                 -->
                    <!-- - - - - - - - - - - - - - - - - - - - - -->
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>

                    <!-- - - - - - - - - - - - - - - - - - - - - -->
                    <!--  Generates a source assembly            -->
                    <!-- - - - - - - - - - - - - - - - - - - - - -->
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-assembly-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>source-release-assembly</id>
                                <phase>package</phase>
                                <goals>
                                    <goal>single</goal>
                                </goals>
                                <configuration>
                                    <!-- Do not execute that plugin for all modules -->
                                    <runOnlyAtExecutionRoot>true</runOnlyAtExecutionRoot>
                                    <descriptorRefs>
                                        <descriptorRef>${ow2SourceAssemblyDescriptorRef}</descriptorRef>
                                    </descriptorRefs>
                                    <!-- Use GNU Tar -->
                                    <tarLongFileMode>gnu</tarLongFileMode>
                                </configuration>
                            </execution>
                        </executions>
                        <dependencies>
                            <dependency>
                                <groupId>org.ow2</groupId>
                                <artifactId>maven-source-assemblies</artifactId>
                                <version>${maven-source-assemblies.version}</version>
                            </dependency>
                        </dependencies>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>

